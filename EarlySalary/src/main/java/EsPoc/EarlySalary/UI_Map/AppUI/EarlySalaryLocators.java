package EsPoc.EarlySalary.UI_Map.AppUI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class EarlySalaryLocators {
	
	// Permissions Screen
	public String verifyCheckBox = "com.earlysalary.android:id/checkbox_verification_tnc"; 	// check box
	public String buttonAgree = "com.earlysalary.android:id/button_next"; 	// I agree
		// negative test
		public String buttonDisagree = "com.earlysalary.android:id/button_skip"; // I disagree
	
	// Permissions Pop-up Dialog 
		// Phone Permissions Text -- For Testing UI logic
		public String textpermissions = "com.earlysalary.android:id/text_view_phone";
		
	public String optionAllow = "com.android.packageinstaller:id/permission_allow_button";
		// negative test
		public String optionDeny = "com.android.packageinstaller:id/permission_deny_button";

	// Tutorial Screen
	public String arrowSkip = "com.earlysalary.android:id/image_view_skip_arrow";

	// Sign-Up or Login Screen
	public String editTextMobileNumber = "com.earlysalary.android:id/edit_text_mobile_number";
	public String buttonGetOTP = "com.earlysalary.android:id/button_next";
	// Appears only after all the permissions are granted
	public String checkBoxIagreeToTC = "com.earlysalary.android:id/checkbox_verification_tnc"; 
	
	// Let's Get Started Screen
	public String editTextOTP = "com.earlysalary.android:id/edit_text_otp";
		public String buttonResendOTP = "com.earlysalary.android:id/button_resend_otp";
	public String buttonLogin = "com.earlysalary.android:id/button_next";
	
	public String buttonNextOn1stLogin = "com.earlysalary.android:id/text_view_got_it";
	public String buttonCloseOn1stLogin = "com.earlysalary.android:id/text_view_notification_got_it";
	public String buttonNextOn1stLoginAtPersonalDetailImg = "com.earlysalary.android:id/text_view_next"; 
	public String buttonCloseOn1stLoginAtPersonalDetailImg = "com.earlysalary.android:id/text_view_next";
	
	// maybe not used
	public String baseLayoutPersonalDetails = "com.earlysalary.android:id/text_view_sub_title_half_tile_second";
	
	public String buttonProceed = "com.earlysalary.android:id/button_next";
	
	// Personal Profile Form
	public String editTextFirstName = "com.earlysalary.android:id/edit_text_first_name";
	public String editTextLastName = "com.earlysalary.android:id/edit_text_last_name";
	public String editTextFathersName = "com.earlysalary.android:id/edit_text_father_name";
	public String editTextMothersName = "com.earlysalary.android:id/edit_text_mother_name";
	public String editTextDD = "com.earlysalary.android:id/edit_text_dd";
	//public String editTextMM = "com.earlysalary.android:id/edit_text_mm";
	//public String editTextYY = "com.earlysalary.android:id/edit_text_yy";
	public String editTextEmail = "com.earlysalary.android:id/edit_text_email";
	public String editTextInstitute = "com.earlysalary.android:id/autocomplete_text_view_institute";
	
	// Profesional Profile Form
	public String editTextCompany = "com.earlysalary.android:id/edit_text_company_name";
	public String editTextDesignation = "com.earlysalary.android:id/edit_text_designation";
	public String editTextOfficeArea = "com.earlysalary.android:id/edit_text_office_area";
	public String editTextOfficeCity = "com.earlysalary.android:id/autocomplete_text_office_city";
	public String editTextPincode = "com.earlysalary.android:id/edit_text_office_pincode";
	public String editTextSalary = "com.earlysalary.android:id/edit_text_monthly_salary";
	public String editTextPAN = "com.earlysalary.android:id/edit_text_pan_number";

	// Confirm Salary Dialog
	public String buttonDialogEdit = "com.earlysalary.android:id/text_view_alert_cancel";
	public String buttonDialogContinue = "com.earlysalary.android:id/text_view_alert_ok";
	
	// Contact Details Form
	public String editTextDoorWingBldg = "com.earlysalary.android:id/edit_text_address_line_one";
	public String editTextStreetLocality = "com.earlysalary.android:id/edit_text_address_line_two";
	public String editTextLandmark = "com.earlysalary.android:id/edit_text_landmark";
	public String editTextCity = "com.earlysalary.android:id/autocomplete_text_view_city";
	public String editTextState = "com.earlysalary.android:id/autocomplete_text_view_state";
	public String editTextPincode2 = "com.earlysalary.android:id/edit_text_pincode";
	
	// Bank Statement
	public String textManualBankStmtUpload = "com.earlysalary.android:id/text_view_manual_title";
	public String uploadFirstMonth = "com.earlysalary.android:id/image_view_month_first";
	public String uploadSecondMonth = "com.earlysalary.android:id/image_view_month_second";
	public String uploadThirdMonth = "com.earlysalary.android:id/image_view_month_third";
	public String editTextpdfPassword = "com.earlysalary.android:id/edit_text_pdf_password";
	public String editTextAutoBank = "com.earlysalary.android:id/autocomplete_text_bank_name";
	public String buttonBrowsePDF = "com.earlysalary.android:id/text_view_pdf_file";
	public String buttonCancel = "com.earlysalary.android:id/text_view_cancel";
	
	// com.earlysalary.android:id/text_view_pdf_file
	// com.earlysalary.android:id/text_view_cancel
	
	// Profession --> [41,490][978,568]
	// Year --> [41,1376][444,1424]
	// Month --> [530,1376][933,1424]
	
	// public String ImageTestXpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.GridLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button";
	// *** Delete below code as it is from the WebUI folder and direct copy-paste for reference ***
	
	
	
	
}
