package EsPoc.EarlySalary.earlySalary;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import EsPoc.EarlySalary.Common.CommonAction.CommonAction;
import EsPoc.EarlySalary.Common.CommonAction.ExcelReader;
import EsPoc.EarlySalary.Common.EsActions.earlySalaryActions;
import EsPoc.EarlySalary.Email.EmailReportManager;
import EsPoc.EarlySalary.ExtentReports.ExtentTestManager;
import EsPoc.EarlySalary.Utilities.SuiteBase;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import EsPoc.EarlySalary.UI_Map.AppUI.EarlySalaryLocators;

//@Listeners(value = EmailReportManager.class)
public class EarlySalaryAppTest extends SuiteBase {

	CommonAction ca = new CommonAction();
	String methodName;
	
	private ExcelReader ex = new ExcelReader();
	private String getDataFromDataProviderSheet = System.getProperty("user.dir")
			+ rd.read_Configfile("earlySalary");
	
	@Test
	public void testCal() throws Exception {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(appDriver, 15);
		Thread.sleep(5000);
		
		EarlySalaryLocators earlySalaryLocators = new EarlySalaryLocators();
		WebElement textElement = appDriver.findElementByAccessibilityId("com.earlysalary.android:id/text_view_phone");
//		textElement = textElement.split(" ",2)[1];
		String element = (earlySalaryLocators.verifyCheckBox).toString();
//		element = textElement.split(" ",2)[1];
		System.out.println("text id --> "+textElement);
		System.out.println("TEXT --> " + (textElement).getText());
		
		boolean found = false;
		
		//new TouchAction((PerformsTouchActions) driver).press(PointOption.point(550, 640)).waitAction().moveTo(PointOption.point(550, 60)).release().perform();
		
		TouchAction action = new TouchAction(appDriver);
	    int height = appDriver.manage().window().getSize().height;
	    int width = appDriver.manage().window().getSize().width;
	    action.press(PointOption.point(width / 2, height / 2))
	            .moveTo(PointOption.point(width / 2, height * 3 / 4)).release().perform();
	    
	    TouchActions action1=new TouchActions(appDriver);
	    action1.scroll(500, 2100).perform();
		
		
		
		
//		System.out.println("Vertical Swipe -- Element Found: "+element);
//		while(found == false) {
//			try {
//			appDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
//			appDriver.findElementByAccessibilityId(element).isDisplayed();
//			}catch(Exception e) {
//				ca.swipeVertical(0.8, 0.2, 2000);
//			}
		
//		ca.swipeVertical(0.1, 0.1, 2000);
//\"com.android.settings:id/content\"
		
		
// Element ID => element.split(" ",2)[1] <= By id: com.earlysalary.android:id/checkbox_verification_tnc
System.out.println(element.split(" ",2)[1]);
MobileElement elementToClick = (MobileElement) appDriver
    .findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()"
        + ".resourceId("+element.split(" ",2)[1]+"))"
        );
elementToClick.click();
		
		}
		

//		WebElement verifyCheckBox=driver.findElement(earlySalaryLocators.verifyCheckBox);
//		verifyCheckBox.click();
		
		
		
	   //locate the Text on the calculator by using By.name()
//	   WebElement two=driver.findElement(By.name("2"));
//	   two.click();
//	   WebElement plus=driver.findElement(By.name("+"));
//	   plus.click();
//	   WebElement four=driver.findElement(By.name("4"));
//	   four.click();
//	   WebElement equalTo=driver.findElement(By.name("="));
//	   equalTo.click();
//	   //locate the edit box of the calculator by using By.tagName()
//	   WebElement results=driver.findElement(By.tagName("EditText"));
//		//Check the calculated value on the edit box
//	assert results.getText().equals("6"):"Actual value is : "+results.getText()+" did not match with expected value: 6";
	}
	
	
//	log.info("here");
	
