package EsPoc.EarlySalary.Utilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import com.nimbusds.oauth2.sdk.Message;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import EsPoc.EarlySalary.Common.CommonAction.CommonAction;
import EsPoc.EarlySalary.Common.CommonAction.ConfigurationManager;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;


public class SuiteBase {
	public static WebDriver driver;
	public static AndroidDriver<MobileElement> appDriver;
//	AppiumDriver<MobileElement> Appdriver;
	public ExtentReports report;
	public ExtentTest logger;
	

	public ConfigurationManager rd = new ConfigurationManager();
	protected static CommonAction ca = new CommonAction();
	public static Logger log = Logger.getLogger("rootLogger");

	@BeforeTest
	public void setupTest() {

		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		String browser = rd.read_Configfile("browser");

		try {

			if (browser.equalsIgnoreCase("firefox")) {
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase("iexplorer")) {
				// Update the driver path with your location
				String IeDriverPath = rd.read_Configfile("IeDriversPath");
				String IeDriverName = rd.read_Configfile("IeDriversName");
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + IeDriverPath + "/" + IeDriverName);
				driver = new InternetExplorerDriver();
				
			} 
			else if(browser.equalsIgnoreCase("chrome"))
	        {
	            // Update the driver path with your location
	            String ChromeDriversPath=rd.read_Configfile("ChromeDriversPath");
	            
	            System.out.println(ChromeDriversPath);
	            String ChromeDriversName=rd.read_Configfile("ChromeDriversName");
	           
	            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ChromeDriversPath+"/"+ChromeDriversName);
	            //Code added for download file
	            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
	    		chromePrefs.put("profile.default_content_settings.popups", 0);
	    		ChromeOptions options = new ChromeOptions();
	    		options.setExperimentalOption("prefs", chromePrefs);
	    		
	    		//DesiredCapabilities cap = DesiredCapabilities.chrome();
	    		//cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
	    		//cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	    		//cap.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
	    		//cap.setCapability(ChromeOptions.CAPABILITY, options);
	    		
	    		options.setAcceptInsecureCerts(true);
	    		options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.IGNORE);
	    		
	    		
	    		driver = new ChromeDriver(options);
	            //driver = new ChromeDriver();
	        } else if(browser.equalsIgnoreCase("app"))
	        {
	        	// Initialize App Driver
	        	
	    		DesiredCapabilities dc = new DesiredCapabilities();
	    		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
	    		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
	    		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "J9AAGF051118DAF"); // Android // J9AAGF051118DAF
	    		dc.setCapability("app", "/Users/karanjhaveri/Downloads/ES_QA_SC.apk");
	    		dc.setCapability("appPackage", "com.earlysalary.android");
//	    		dc.setCapability("appActivity", "com.earlysalary.android.activity.ESHomeActivity");
	    		// Permission is denied if trying to access Activity directly
	    		
//	    		URL url = new URL("http://localhost:4723/wd/hub"); // For Windows OS
	    		URL url = new URL("http://0.0.0.0:4723/wd/hub"); // For Mac OS
	    		appDriver = new AndroidDriver<MobileElement>(url, dc);
	        }
			
			log.info("Browser Started");
			// screen.stop();
			driver.manage().window().maximize();
			driver.get(rd.read_Configfile("url"));
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		} 
			catch (Exception e) {
			System.out.println(e);
		}

	}

	@AfterTest
	public void tearownTest() {
		driver.quit();

	}
	
	public WebDriver getDriver() {
        return driver;
    }

	public String generateReport(String reportFileName, String reportDocTitle, String reportName,
			String reportDetailsName) {
		// System.out.println("Report for Adgebra.");
		// String reportURL =
		// CreativeServerImages.ReportPath+"/"+reportFileName+".html";
		// String reportURL =
		// "/home/amol/git/selfserve_adgebraui_selenium/SelfServ-Selenium/report/"+reportFileName+".html";
		String reportURL = "./report/" + reportFileName + ".html";
		report = new ExtentReports(reportURL, true);
		report.config().documentTitle(reportDocTitle);
		report.config().reportName(reportName);
		logger = report.startTest(reportDetailsName + " Tests");
		return reportURL;
	}

	public void captureFailureScreen(String methodName) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(Calendar.getInstance().getTime());
		String srcfile = "./ScreenShot/" + methodName + "_" + timeStamp + ".jpg";
		System.out.println("srcfile :" + srcfile);
		try {
			FileUtils.copyFile(scrFile, new File(srcfile));
			//logger.log(LogStatus.FAIL, logger.addScreenCapture(srcfile));
		} catch (IOException e) {
			System.out.println("Scrren not capture..");
		}
	}

}
