package EsPoc.EarlySalary.earlySalary;

import java.sql.Connection;
import java.sql.DriverManager;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

public class DSLContextCreatorEarlysalary {
	private static DSLContext dslContext;

	private static DSLContextCreatorEarlysalary dslContextCreatorEarlysalary;

	private DSLContextCreatorEarlysalary() {
		super();
	}

	public static DSLContextCreatorEarlysalary getInstance() {
		if (dslContextCreatorEarlysalary == null) {
			dslContextCreatorEarlysalary = new DSLContextCreatorEarlysalary();
		}
		return dslContextCreatorEarlysalary;
	}

	public static DSLContext dslContext() {
		
		String userName = "qa_user".trim();
		String password = "Quality@#$54".trim();
		String url = "jdbc:mysql://cashcare-qa.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:3306/earlysalary".trim();
		
		if (dslContext == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection conn = (Connection) DriverManager.getConnection(url, userName, password);
				dslContext = DSL.using(conn, SQLDialect.MYSQL);
				return dslContext;
			} catch (Exception e) {
				System.out.println("Exception is " + e.getMessage());
				e.printStackTrace();
			}
		}
		return dslContext;
	}
}
