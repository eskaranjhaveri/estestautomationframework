package EsPoc.EarlySalary.UI_Map.WebUI;

import org.openqa.selenium.By;

public class EarlySalaryLocators {

	//EarlySalaryLoginpage
	public By enterMobileNumber = By.id("bankname");
	public By termCondictionCheckbox = By.xpath(".//input[@class='sc-cJSrbW dYJjgH']");
	public By signUpButton = By.xpath(".//span[@class='sc-bSbAYC iPiyjo']");			
	public By enterOTP = By.xpath(".//input[@class='jss23 jss26'][@id='otp']");
	public By verifyOTPbutton = By.xpath(".//button[@class='sc-jhaWeW lzbSw'][2]/span");
	public By verifyLoginText = By.xpath(".//span[@class='sc-ktHwxA gyZHVZ']");
	public By verifyLoginTextFirstTime = By.xpath(".//span[@class='sc-fgrSAo hZMeyQ']");

	
	
	
	//Unable to verify OTP, please try again
	public By alertMessageForLoginFail = By.xpath(".//span[@class='sc-dnqmqq cyaDPN']");
	public By alertMessageConfirmButton = By.xpath(".//span[@class='sc-gZMcBi kOGJfe']");
	

	//Personal Profile
	public By EnterFirstName = By.id("first_name");
	public By EnterLastName = By.id("last_name");
	public By EnterFatherName = By.id("fathers_name");
	public By EnterMotherName = By.id("mothers_name");
	public By GenderFemale = By.id("female"); // .//span[@class='jss204 jss198 jss194 jss189 jss192']/span[1]
//	public By GenderMale = By.xpath(".//input[@id='male']"); // .//span[@class='jss204 jss198 jss194 jss189 jss192 jss195 jss190']/span[1]
	public By GenderMale = By.xpath(".//span[@class='jss204 jss198 jss194 jss189 jss192 jss195 jss190']/span[1]"); 
	
	
	public By EnterDateOfBirth = By.id("date-of-birth");
	public By SelectDay = By.id("day");
	public By SelectDayList = By.xpath(".//div[@id='menu-day']/div[2]/ul/li");
	public By SelectMonth = By.id("month");
	public By SelectMonthList = By.xpath(".//div[@id='menu-month']/div[2]/ul/li");
	public By SelectYear = By.id("year");
	public By SelectYearList = By.xpath(".//div[@id='menu-year']/div[2]/ul/li");
	
	public By EnterEmailID = By.id("email");
	public By SelectHighestEducation = By.xpath(".//div[@class='jss287 jss288 jss179']");
	public By SelectHighestEducationList = By.xpath(".//div[@id='menu-hEdu']/div[2]/ul/li");
	
	public By EnterInstituteName = By.xpath(".//input[@placeholder=' Select Institute Name '][@type='text']");
	
	public By SelectMaritalStatus = By.id("menu-rStatus");
	public By SelectMaritalStatusList = By.xpath(".//div[@id='menu-rStatus']/div[2]/ul/li");
	public By PersonalProfileNext = By.xpath(".//span[@class='sc-gPEVay lbCgCs']");
	public By PersonalProfileConfirmation = By.xpath(".//span[@class='sc-blIhvV bTRwVX']");
	
	
	
}
