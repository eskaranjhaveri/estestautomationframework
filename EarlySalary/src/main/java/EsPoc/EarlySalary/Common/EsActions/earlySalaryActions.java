package EsPoc.EarlySalary.Common.EsActions;

import java.net.MalformedURLException;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import EsPoc.EarlySalary.Common.CommonAction.CommonAction;
import EsPoc.EarlySalary.UI_Map.WebUI.EarlySalaryLocators;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class earlySalaryActions {

	WebDriver driver;
	WebDriverWait wait;
	
	AndroidDriver<MobileElement> appDriver ;
	
	String callerClassName = Thread.currentThread().getStackTrace()[2].getMethodName();
	
	EarlySalaryLocators esLoc = new EarlySalaryLocators();
	EsPoc.EarlySalary.UI_Map.AppUI.EarlySalaryLocators eslocApp = new EsPoc.EarlySalary.UI_Map.AppUI.EarlySalaryLocators();
	CommonAction ca = new CommonAction();
	EarlySalaryOTP otp = new EarlySalaryOTP();
	
	public earlySalaryActions(WebDriver driver, WebDriverWait wait)
	{
		this.driver = driver;
		this.wait = wait;
	}
	
	public String TC_EarlySalaryLogin(String methodName, Hashtable<String, String> sheetData, ExtentTest logger) throws InterruptedException, MalformedURLException
	{
		driver.navigate().refresh();
		ca.ElementClear(esLoc.enterMobileNumber, driver, wait);
		ca.ElementToSendDataLogger(esLoc.enterMobileNumber, sheetData.get("Mobile_Number"), driver, wait, logger, "Steps 1");
		ca.ElementToClickWithLogger(esLoc.termCondictionCheckbox, driver, wait, logger, "Steps 2");
		ca.ElementToClickWithLogger(esLoc.signUpButton, driver, wait, logger, "Steps 3");
		//Thread.sleep(2000);
		ca.ElementToSendDataLogger(esLoc.enterOTP, otp.GetOTPValue(), driver, wait, logger, "Steps 5");
		ca.ElementToClickWithLogger(esLoc.verifyOTPbutton, driver, wait, logger, "Steps 6");
		Thread.sleep(5000);
		String getLogginText= ca.ElementGetTextLogger(esLoc.verifyLoginTextFirstTime, driver, wait, logger, "Steps 7");
		System.out.println( "loggin success text :" +getLogginText);
		return getLogginText;
	}
	
	public String TC_LoginFailConfirmation(String methodName, Hashtable<String, String> sheetData, ExtentTest logger) throws InterruptedException
	{
		ca.ElementClear(esLoc.enterMobileNumber, driver, wait);
		ca.ElementToSendDataLogger(esLoc.enterMobileNumber, sheetData.get("Mobile_Number"), driver, wait, logger, "Steps 1");
		ca.ElementToClickWithLogger(esLoc.termCondictionCheckbox, driver, wait, logger, "Steps 2");
		ca.ElementToClickWithLogger(esLoc.signUpButton, driver, wait, logger, "Steps 3");
		Thread.sleep(10000);
		ca.ElementToSendDataLogger(esLoc.enterOTP, sheetData.get("OTP"), driver, wait, logger, "Steps 4");
		ca.ElementToClickWithLogger(esLoc.verifyOTPbutton, driver, wait, logger, "Steps 5");
		
		String getConfirmationText = ca.ElementGetTextLogger(esLoc.alertMessageForLoginFail, driver, wait, logger, "Steps 6");
		ca.ElementToClickWithLogger(esLoc.alertMessageConfirmButton, driver, wait, logger, "Steps 7");
		return getConfirmationText;
		
	}
	
	public String TC_PersonalProfileValidation(String methodName, Hashtable<String, String> sheetData, ExtentTest logger) throws InterruptedException
	{
		ca.ElementToSendDataLogger(esLoc.EnterFirstName, sheetData.get("First_Name"), driver, wait, logger, "Steps 1");
		ca.ElementToSendDataLogger(esLoc.EnterLastName, sheetData.get("Last_Name"), driver, wait, logger, "Steps 2");
		ca.ElementToSendDataLogger(esLoc.EnterFatherName, sheetData.get("Father_Name"), driver, wait, logger, "Steps 4");
		ca.ElementToSendDataLogger(esLoc.EnterMotherName, sheetData.get("Mother_Name"), driver, wait, logger, "Steps 5");
		ca.ElementToClickWithLogger(esLoc.GenderMale, driver, wait, logger, "Steps 6");
		
		ca.ElementToSendDataLogger(esLoc.EnterDateOfBirth, sheetData.get("Date_of_Birth"), driver, wait, logger, "Steps 6");
		ca.ElementToSendDataLogger(esLoc.EnterEmailID, sheetData.get("Email_ID"), driver, wait, logger, "Steps 7");
		ca.SelectElementFromList(esLoc.SelectHighestEducation, esLoc.SelectHighestEducationList, sheetData.get("Highest_Education"), driver, wait, logger, "Steps 8");;
		ca.SelectElementFromList(esLoc.SelectMaritalStatus, esLoc.SelectMaritalStatusList, sheetData.get("Marital_Status"), driver, wait, logger, "Steps 9");
		ca.ElementToSendDataLogger(esLoc.EnterInstituteName, sheetData.get("InstituteName"), driver, wait, logger, "Steps 10");
		
		ca.ElementToClickWithLogger(esLoc.PersonalProfileNext, driver, wait, logger, "Steps 11");
		String confirmationtext = ca.ElementGetTextLogger(esLoc.PersonalProfileConfirmation, driver, wait, logger, "Steps 11");
		return confirmationtext;
				 
	}
	
	public String TC_LoginConfirmationApp(String methodName, Hashtable<String,String> objects) throws InterruptedException
	{
		ca.ElementToSendDataApp(eslocApp.editTextMobileNumber, objects.get("Mobile_Number"), appDriver);
		//ca.ElementToSendDataApp(eslocApp.editTextMobileNumber, sheetData.get("Mobile_Number"), driver, wait); 
		
		
		
		//String getConfirmationText = ca.ElementGetTextLogger(esLoc.alertMessageForLoginFail, driver, wait, "Steps 6");
		return methodName;
		
	}
	
	
}






