package EsPoc.EarlySalary.earlySalary;


import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;
public class ConnectDB {

	private static ComboPooledDataSource cpds =null;
	public static Connection getConnection() throws SQLException{
		if(cpds==null)
			cpds=makePool();
		return cpds.getConnection() ;
	}
	
	public static ComboPooledDataSource makePool(){ 	 
		if(cpds==null)
			cpds=new ComboPooledDataSource();
		try{
		cpds.setDriverClass( "com.mysql.jdbc.Driver" ); //loads the jdbc driver 
		/*cpds.setJdbcUrl("jdbc:mysql://es.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:7011/earlysalary?autoReconnect=true");
		cpds.setUser("pg");
		cpds.setPassword("PGs@1Ab@$");*/
		
		cpds.setJdbcUrl("jdbc:mysql://cashcare-qa.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:3306/earlysalary");
		cpds.setUser("qa_user");
		cpds.setPassword("Quality@#$54");//Qa@2015##$$
		cpds.setMaxPoolSize(5);
		}catch(Exception e){
			e.printStackTrace();
		}
		return cpds;
	}

	public static Connection getConnectionLMS() throws SQLException{
		if(cpds==null)
			cpds=makePoolLMS();
		return cpds.getConnection();
	}
	
	public static ComboPooledDataSource makePoolLMS(){ 	 
		if(cpds==null)
			cpds=new ComboPooledDataSource();
		try{
		cpds.setDriverClass( "com.mysql.jdbc.Driver" ); //loads the jdbc driver 
//		cpds.setJdbcUrl("jdbc:mysql://es.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:7011/earlysalary?autoReconnect=true");
//		cpds.setUser("pg");
//		cpds.setPassword("PGs@1Ab@$");
		
		cpds.setJdbcUrl("jdbc:mysql://cashcare-qa.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:3306/es_lms");
		cpds.setUser("qa_user");
		cpds.setPassword("Quality@#$54");//Qa@2015##$$
		cpds.setMaxPoolSize(5);
		}catch(Exception e){
			e.printStackTrace();
		}
		return cpds;
	}
	
}
