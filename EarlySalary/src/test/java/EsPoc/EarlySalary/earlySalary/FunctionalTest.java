package EsPoc.EarlySalary.earlySalary;

import java.awt.Point;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Duration;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;

//import org.apache.commons.logging.impl.Log4JLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.jooq.DSLContext;
import EsPoc.EarlySalary.earlySalary.DSLContextCreatorEarlysalary;

import com.relevantcodes.extentreports.ExtentTest;

import EsPoc.EarlySalary.Common.CommonAction.CommonAction;
import EsPoc.EarlySalary.Common.CommonAction.ConfigurationManager;
import EsPoc.EarlySalary.Common.CommonAction.ExcelReader;
import EsPoc.EarlySalary.Common.EsActions.EarlySalaryOTP;
import EsPoc.EarlySalary.Common.EsActions.earlySalaryActions;
import EsPoc.EarlySalary.UI_Map.AppUI.EarlySalaryLocators;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class FunctionalTest {

	static AndroidDriver<MobileElement> driver;
	EarlySalaryLocators esLoc = new EarlySalaryLocators();
		
	public static String GetOTPValue() throws MalformedURLException, InterruptedException
	{
		
		CommonAction ca = null;
		
		AndroidDriver<MobileElement> appDriver;
		AppiumDriver<MobileElement> appDriver2;
//		AppiumDriver<MobileElement> Appdriver;
		EarlySalaryLocators earlySalaryLocators = new EarlySalaryLocators();
		boolean socialMethod = false;
		boolean methodSkip = false;
		int method = 1;
		
		/*
		 APPIUM ACTIVITY DETAILS FROM TERMINAL
		 adb shell dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'
		 */
		
		/*
			RUN APPIUM SERVER WITH AUTO GRANT PERMISSIONS
			{
	  		"platformName": "Android",
	  		"deviceName": "J9AAGF051118DAF",
	  		"app": "/Users/karanjhaveri/Downloads/ES_QA_SC_2_1_6.apk",
	  		"appWaitPackage": "com.earlysalary.android",
	  		"appWaitActivity": "com.earlysalary.android.activity.ESSplashActivity",
			"autoGrantPermissions":true
		}
		 */
		
		try {
		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
		// J9AAGF051118DAF // Android // 0123456789ABCDEF
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "J9AAGF051118DAF");
		// Comment below line as the app needs to not install everytime.
		dc.setCapability("app", "/Users/karanjhaveri/Downloads/ES_QA_SC_2_1_6.apk");
		dc.setCapability("appPackage", "com.earlysalary.android");
		dc.setCapability("autoGrantPermissions", true);
//		dc.setCapability("appActivity", "com.google.android.apps.messaging.ui.conversationlist.ConversationListActivity");
	
//		URL url = new URL("http://localhost:4723/wd/hub"); // For Windows OS
		URL url = new URL("http://0.0.0.0:4723/wd/hub"); // For Mac OS
		appDriver = new AndroidDriver<MobileElement>(url, dc);
		TouchAction action= new TouchAction(appDriver);
//		WebDriverWait wait = new WebDriverWait(appDriver, 15);
//		appDriver2 = new AppiumDriver<MobileElement>(url, dc);
		
		// The wait is kept to skip the loading screen
		try {
			appDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// Below line inspects UI to understand whether it is not in 1st launch?
		if(appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Mobile Number"+"\").instance(0))").isDisplayed()) {
			// THIS IS RUN WHEN PERMISSIONS ARE GRANTED
			System.out.println("20 second wait");
		}
		}catch(Exception e) {
			// RUN THIS ON FIRST LAUNCH WHEN PERMISSIONS ARE NOT GRANTED
			System.out.println("60 second wait");
			appDriver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			// P.S. You ran 20 seconds wait before so this wait is actually 60 seconds
		}

        // Get the size of screen.
		/*
		Dimension size = appDriver.manage().window().getSize();
        System.out.println(size);
		*/
		
		// DEBUG: Have I reached permissions screen yet?
		try {
		if(appDriver.findElementById("com.earlysalary.android:id/text_view_phone").isDisplayed()) {
		String text = appDriver.findElementById("com.earlysalary.android:id/text_view_phone").getText();
		System.out.println(text);
		appDriver.findElementById(earlySalaryLocators.verifyCheckBox).click();
		// "I, AGREE" -- Scroll and Click Text
		appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"I, AGREE"+"\").instance(0))").click();
		// ONLY RUN IF PERMISSION IS NOT GRANTED
		for(int i=0; i<4; i++) {
			appDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			appDriver.findElementById(earlySalaryLocators.optionAllow).click();
			//appDriver.findElementByAccessibilityId("ALLOW").click(); 
		}
		appDriver.findElementById(earlySalaryLocators.arrowSkip).click();
		}
		}catch(Exception e) {
			System.out.println("Permissions Screen Skipped");
		}
		
		// element = element.split(" ",2)[1]; // To Access the element ID From 'By.name(id)'
		
		/* ExcelReader Logic - Wasim will share */
		// earlySalaryActions esa = null;
		// EarlySalaryTest est = null;
		// String methodName = null;
		// Log4JLogger log = new Log4JLogger();
		//esa.TC_LoginConfirmationApp(methodName, est.getLoginData());
		
		//est.verifyValidLogin( est.getLoginData(),"");

		// from :: SuiteBase
		
		//ConfigurationManager rd = new ConfigurationManager();
		
		//from :: earlySalaryTest.java Class
		
//		ExcelReader ex = new ExcelReader();
//		String getDataFromDataProviderSheet = System.getProperty("user.dir")
//				+ rd.read_Configfile("earlySalary");
//		
//		Object [][] LoginData = ex.getDataingrid(getDataFromDataProviderSheet, "TC_AutoLogin_Data");
//		
//		Hashtable<String, String> sheetData;
//		for(int i=0; i<LoginData.length; i++) {
////			sheetData.put(Object[i][0], Object[i][0]);
//			sheetData.put(LoginData[i].toString(), LoginData[i][0].toString());
//		}
//		
//		
//		ca.ElementToSendDataApp(By.id(earlySalaryLocators.editTextMobileNumber), sheetData.get("Mobile_Number"), appDriver, wait);

		String activity = appDriver.currentActivity();
		System.out.println("Login / Sign up Activity Name : "+activity);
		
		//appDriver.findElementById(earlySalaryLocators.editTextMobileNumber).click();
		appDriver.findElementById(earlySalaryLocators.editTextMobileNumber).clear();
		appDriver.findElementById(earlySalaryLocators.editTextMobileNumber).sendKeys("9004771984");
		appDriver.navigate().back();

		try {
		if(appDriver.findElementById(earlySalaryLocators.checkBoxIagreeToTC).isDisplayed()) {
			appDriver.findElementById(earlySalaryLocators.checkBoxIagreeToTC).click();
		}
		}catch(Exception e) {
			System.out.println("I agree to T&C (checkbox) is not displayed");
		}
			
		// System.out.println(appDriver.getPageSource());
		
		appDriver.findElementById(earlySalaryLocators.buttonGetOTP).click();
		
		
		// if prompted with internet is slow & retry OTP
		try {
		if(appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"RETRY"+"\").instance(0))").isDisplayed()) {
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"RETRY"+"\").instance(0))").click();
		}
		}catch(Exception e) {} // Do nothing
		
		Thread.sleep(5000);
		appDriver.findElementById(earlySalaryLocators.buttonLogin).click();
		
		// Social Login Method
		try {
		if(appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Sign"+"\").instance(0))").isDisplayed()) {
			System.out.println("Social Login Entered");
			action = new TouchAction(appDriver);
			// Press Sign in with Google
			action.press(PointOption.point(550, 1650)).release().perform();
			
			// Press Google Account -- @earlysalary.com
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"@earlysalary.com"+"\").instance(0))").click();
			// Press Google Account with co-ordinates | Bounding Box for 1st A/C : [231,958][951,1056] */
			
			// UI Tutorial on Sign Up / 1st Login
			appDriver.findElementById(earlySalaryLocators.buttonNextOn1stLogin).click();
			appDriver.findElementById(earlySalaryLocators.buttonCloseOn1stLogin).click();
			
			socialMethod = true;
			
//			Thread.sleep(5000);
			
			System.out.println("Social Login Cleared");
		}
		}catch(Exception e) {} // Do Nothing
		
//		Thread.sleep(5000);
//		appDriver.findElement(By.name("Continue")).click();
		
		Thread.sleep(3000);
//		try {
		if(socialMethod == true) {
			// select with coordinates -- TEXT: GO
			action = new TouchAction(appDriver);
			action.press(PointOption.point(900, 1950)).release().perform();
		} else {
			// Tap on BaseLayout -- Text: Personal Details
			try {
			if(appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Personal Details"+"\").instance(0))").isDisplayed())
				appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Personal Details"+"\").instance(0))").click();
			}catch(Exception e) {
				methodSkip = true; // Go to another step -- not personal details one
				method = 2;
			}
		}			
		//appDriver.findElementByName("Go").click();
		
		if(socialMethod==true) {
			// do the social tutorial steps on Personal Details Image Screen
			appDriver.findElementById(earlySalaryLocators.buttonNextOn1stLoginAtPersonalDetailImg).click();
			appDriver.findElementById(earlySalaryLocators.buttonCloseOn1stLoginAtPersonalDetailImg).click();
		}
		
		/* TODO: NEED TO ADD TEST CASES (VALIDATION) TO CHECK IF INVALID ENTRIES ARE ACCEPTED */
		
		if(methodSkip==false) { 
			Thread.sleep(3000);
			
			// Tap on Personal Details Image
			
			/* TODO: IF THE USER IS LOGGED IN AFTER 1ST TIME AND IS ON FIRST TILE 
			 * -- WE CAN SKIP BELOW 2 LINES MAYBE -- CONFIRM MANUALLY
			 * -- AS IT MIGHT DIRECTLY REDIRECT TO THE 1ST TILE
			 */
			
			action = new TouchAction(appDriver);
			action.press(PointOption.point(530, 750)).release().perform();
			
			// Personal Details Form
			appDriver.findElementById(earlySalaryLocators.editTextFirstName).clear();
			appDriver.findElementById(earlySalaryLocators.editTextFirstName).sendKeys("Karan");
			//appDriver.hideKeyboard();
			appDriver.findElementById(earlySalaryLocators.editTextLastName).clear();
			appDriver.findElementById(earlySalaryLocators.editTextLastName).sendKeys("Jhaveri");
			//appDriver.hideKeyboard();
			appDriver.findElementById(earlySalaryLocators.editTextFathersName).clear();
			appDriver.findElementById(earlySalaryLocators.editTextFathersName).sendKeys("Dad");
			//appDriver.hideKeyboard();
			appDriver.findElementById(earlySalaryLocators.editTextMothersName).clear();
			appDriver.findElementById(earlySalaryLocators.editTextMothersName).sendKeys("Mom");
			//appDriver.hideKeyboard();

			//appDriver.findElementById(earlySalaryLocators.editTextDD).clear();
			appDriver.findElementById(earlySalaryLocators.editTextDD).sendKeys("01121950");
			//Thread.sleep(3000);
			appDriver.findElementById(earlySalaryLocators.editTextDD).click();
			appDriver.navigate().back();
			//appDriver.hideKeyboard();
			//Thread.sleep(3000);
			
			appDriver.findElementById(earlySalaryLocators.editTextEmail).clear();
			//Thread.sleep(3000);
			appDriver.findElementById(earlySalaryLocators.editTextEmail).sendKeys("karan.jhaveri@earlysalary.com");
			appDriver.findElementById(earlySalaryLocators.editTextEmail).click();
			appDriver.navigate().back();
			//appDriver.hideKeyboard();
			
			appDriver.findElementById(earlySalaryLocators.editTextInstitute).clear();
			appDriver.findElementById(earlySalaryLocators.editTextInstitute).sendKeys("University of Mumbai");
			appDriver.findElementById(earlySalaryLocators.editTextInstitute).click();
			appDriver.navigate().back();
			//appDriver.hideKeyboard();
			
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Gender"+"\").instance(0))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Male"+"\").instance(0))").click();
	
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Marital"+"\").instance(0))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Single"+"\").instance(0))").click();
	
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Education"+"\").instance(0))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Undergraduate"+"\").instance(0))").click();
			
			appDriver.findElementById(earlySalaryLocators.buttonProceed).click();
			
			Thread.sleep(1000);
		}
		
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
		
		if(methodSkip==true) {
			
			/* WE SKIPPED 1ST TILE BUT ADD LOGIC TO CHECK IF ITS SUPPOSED TO SKIP 2ND TILE 
			 * AS WELL AND GO TO 3RD TILE
			 * OR STAY AT 2ND TILE
			 */
			appDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				if(appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Contact Details"+"\").instance(0))").isDisplayed()) {
					method = 3;
				}
			}catch(Exception e1){
				method = 2;
				
				try {
					if(appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Bank Statement"+"\").instance(0))").isDisplayed()) {
						method = 4;
					}
				}catch(Exception e2) {}
				
			}
			
			// Then, Click on the Professional Detail banner or Contact Detail banner
			// select with coordinates -- TEXT: Continue
			action = new TouchAction(appDriver);
			action.press(PointOption.point(900, 1950)).release().perform();
			Thread.sleep(1500);
		}
		
		methodSkip = false;
		
		// Professional Profile Form
		if(method == 2) {
			appDriver.findElementById(earlySalaryLocators.editTextCompany).clear();
			appDriver.findElementById(earlySalaryLocators.editTextCompany).sendKeys("EARLYSALARY");
			Thread.sleep(1500);
			appDriver.pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
			appDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
			// Deprecated Code :-
//			appDriver.pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
//			appDriver.pressKeyCode(AndroidKeyCode.ENTER);
			//appDriver.findElementById(earlySalaryLocators.editTextDesignation).click(); // Company Text DD
			
			appDriver.findElementById(earlySalaryLocators.editTextDesignation).clear();
			appDriver.findElementById(earlySalaryLocators.editTextDesignation).sendKeys("Technical Architect");
			appDriver.findElementById(earlySalaryLocators.editTextOfficeArea).clear();
			appDriver.findElementById(earlySalaryLocators.editTextOfficeArea).sendKeys("Viman Nagar");
			appDriver.findElementById(earlySalaryLocators.editTextOfficeCity).clear();
			appDriver.findElementById(earlySalaryLocators.editTextOfficeCity).sendKeys("Pune");
			appDriver.pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
			appDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
			// Deprecated Code :-
//			appDriver.pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
//			appDriver.pressKeyCode(AndroidKeyCode.ENTER);
			//appDriver.findElementById(earlySalaryLocators.editTextPincode).click(); // City Text DD
							
			// Pincode number of digits check
			appDriver.findElementById(earlySalaryLocators.editTextPincode).clear();
			appDriver.findElementById(earlySalaryLocators.editTextPincode).sendKeys("411014");
			
			appDriver.findElementById(earlySalaryLocators.editTextPincode).click();
			appDriver.navigate().back();
			
			// Monthly Salary cannot be more than 7 digits
			appDriver.findElementById(earlySalaryLocators.editTextSalary).clear();
			appDriver.findElementById(earlySalaryLocators.editTextSalary).sendKeys("1098765");
			appDriver.findElementById(earlySalaryLocators.editTextPincode).click(); // click away
			
			// Confirm Monthly Salary Dialog --> Edit or Continue Buttons
			appDriver.findElementById(earlySalaryLocators.buttonDialogContinue).click();
			
			appDriver.findElementById(earlySalaryLocators.editTextPincode).click();
			appDriver.navigate().back();
			
			// https://www.quora.com/What-is-the-significance-of-each-letter-mentioned-on-pan-card
			
			appDriver.findElementById(earlySalaryLocators.editTextPAN).clear();
			appDriver.findElementById(earlySalaryLocators.editTextPAN).sendKeys("ABCPD1234E");
			
			appDriver.findElementById(earlySalaryLocators.editTextPAN).click();
			appDriver.navigate().back();
			
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Profession"+"\").instance(1))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Salaried"+"\").instance(0))").click();
			
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Year"+"\").instance(0))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"20"+"\").instance(0))").click();
			
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Month"+"\").instance(0))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"9"+"\").instance(0))").click();
			appDriver.findElementById(earlySalaryLocators.buttonProceed).click();
			
			method = 3;
		}
		
		// Current Residential Address Form
		if(method == 3) {
			appDriver.findElementById(earlySalaryLocators.editTextDoorWingBldg).sendKeys("Apt. 1, Wing A, EarlySalary Bldg");
			appDriver.findElementById(earlySalaryLocators.editTextStreetLocality).sendKeys("Ganpati Chowk, V.N. Road");
			appDriver.findElementById(earlySalaryLocators.editTextLandmark).sendKeys("Panchshil Chambers");
			appDriver.findElementById(earlySalaryLocators.editTextCity).sendKeys("Pune");
			appDriver.pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
			appDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
//			appDriver.pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
//			appDriver.pressKeyCode(AndroidKeyCode.ENTER);
			// appDriver.findElementById(earlySalaryLocators.editTextState).click(); // click state
			appDriver.findElementById(earlySalaryLocators.editTextState).sendKeys("Maharashtra");
			appDriver.pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
			appDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
//			appDriver.pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
//			appDriver.pressKeyCode(AndroidKeyCode.ENTER);
			//appDriver.findElementById(earlySalaryLocators.editTextPincode2).click(); // click pincode
			appDriver.findElementById(earlySalaryLocators.editTextPincode2).sendKeys("411014");
			
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Residential Type"+"\").instance(0))").click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+"Rented"+"\").instance(0))").click();
			
			appDriver.findElementById(earlySalaryLocators.editTextPincode2).click(); // click pincode
			appDriver.navigate().back();
			
			appDriver.findElementById(earlySalaryLocators.buttonProceed).click();
			
			method = 4;
		}
		
		// Bank Statement Upload
		if(method == 4) {
			
			// UPDATE THESE FILE NAMES (BASED ON DEVICE)
			String bankStatement1 = "Technical Stock Idea - 01.11.18.pdf";
			String bankStatement2 = "Technical Stock Idea - 01.11.18.pdf";
			String bankStatement3 = "Technical Stock Idea - 01.11.18.pdf";
			
			Thread.sleep(2000); // can be commented -- ju
			action = new TouchAction(appDriver);
			action.press(PointOption.point(900, 1950)).release().perform();
			
			appDriver.findElementById(earlySalaryLocators.textManualBankStmtUpload).click();
			
			//uploadFirstMonth
			appDriver.findElementById(earlySalaryLocators.uploadFirstMonth).click();
			
			appDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			
			try {
				// Permissions
				if(appDriver.findElementById(earlySalaryLocators.optionAllow).isDisplayed()) {
					appDriver.findElementById(earlySalaryLocators.optionAllow).click();
					appDriver.findElementById(earlySalaryLocators.uploadFirstMonth).click();
					Thread.sleep(1000);
					action = new TouchAction(appDriver);
					action.press(PointOption.point(1000, 1250)).release().perform();
					// Proceed to upload documents
					appDriver.findElementById(earlySalaryLocators.uploadFirstMonth).click();
				}
			}catch(Exception e) {}
			
			appDriver.findElementById(earlySalaryLocators.buttonBrowsePDF).click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+bankStatement1+"\").instance(0))").click();
			
			//uploadSecondMonth
			appDriver.findElementById(earlySalaryLocators.uploadSecondMonth).click();
			appDriver.findElementById(earlySalaryLocators.buttonBrowsePDF).click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+bankStatement2+"\").instance(0))").click();
			
			//uploadThirdMonth
			appDriver.findElementById(earlySalaryLocators.uploadThirdMonth).click();
			appDriver.findElementById(earlySalaryLocators.buttonBrowsePDF).click();
			appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+bankStatement3+"\").instance(0))").click();
			
			//opt : editTextpdfPassword
						
			//appDriver.findElementById(earlySalaryLocators.editTextAutoBank).isDisplayed();
			
			appDriver.findElementById(earlySalaryLocators.editTextAutoBank).clear();
			appDriver.findElementById(earlySalaryLocators.editTextAutoBank).sendKeys("IDFC");
			appDriver.pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
			appDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
			
			// Ensure File Selector is in ListView and not GridView by default
						
			// Deprecated Code :-
			/* appDriver.pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
			/ appDriver.pressKeyCode(AndroidKeyCode.ENTER); */
			
			// editTextAutoBank
			
			appDriver.findElementById(earlySalaryLocators.buttonProceed).click();
			
		}
		
		/* OTP DB LOGIC
		//appDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//String fileExtractionQuery = "select otp_code from earlysalary.otp_log_detail where mdn = 9004771984 and status = \"DELIVERED\" order by id desc limit 1;";
		//PreparedStatement preparedStatement = null;
		//Connection conn = null;
		//ConnectDB connectDB = new ConnectDB();
		//conn = ConnectDB.getConnection();
		
		//DSLContext dslContext = DSLContextCreatorEarlysalary.dslContext();
		//preparedStatement = conn.prepareStatement(fileExtractionQuery);
		//ResultSet result= preparedStatement.executeQuery();
		//int otp = result.getInt("otp_code");
		
		//appDriver.findElementById(earlySalaryLocators.editTextOTP).click();
		//appDriver.findElementById(earlySalaryLocators.editTextOTP).sendKeys(""+otp);
		*/
		
		//Thread.sleep(3000);
		
		//appDriver.findElementById(earlySalaryLocators.baseLayoutPersonalDetails).click();
		//Thread.sleep(5000);
		
		//appDriver.findElementByXPath(earlySalaryLocators.ImageTestXpath).click();
		
		//\"com.android.settings:id/content\"
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
//com.google.android.apps.messaging:id/conversation_name
	    
	}
	
	
	public static void main(String[] argc) throws Exception
	{
		GetOTPValue();
		//EarlySalaryOTP obj = new EarlySalaryOTP();
		//obj.GetOTPValue();
	
	}
	
	/*
	 * REFERENCE:
	 * 
	 * https://developers.perfectomobile.com/pages/viewpage.action?pageId=31103222
	 * https://stackoverflow.com/questions/55677189/how-to-scroll-in-android-using-java-appium-client
	 * https://stackoverflow.com/questions/27058183/how-to-scroll-down-a-page-in-appium
	 * 
	 */
}
