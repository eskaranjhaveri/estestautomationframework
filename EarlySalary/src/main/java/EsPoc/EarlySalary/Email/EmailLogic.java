
package EsPoc.EarlySalary.Email;


import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import EsPoc.EarlySalary.Common.CommonAction.ConfigurationManager;
public class EmailLogic 
{
	 public void sendMail(String emailContent) {
	        //Setting up configurations for the email connection to the Google SMTP server using TLS
	        Properties props = new Properties();
	        props.put("mail.smtp.host", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587"); //587 596 443 995 465
	        props.put("mail.smtp.auth", "true");
	        //Establishing a session with required user details
	        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication("wasim.shaikh@earlysalary.com", "Wasim@88");
	            }
	        });
	        try {
	            //Creating a Message object to set the email content
	            MimeMessage message = new MimeMessage(session);
	            //Storing the comma seperated values to email addresses
	            String to = "wasim.shaikh@earlysalary.com";
	          //  Parsing the String with defualt delimiter as a comma by marking the boolean as true and storing the email
	            //addresses in an array of InternetAddress objects
	            InternetAddress[] address = InternetAddress.parse(to, true);
	            
	            //Setting the recepients from the address variable
	            message.setRecipients(Message.RecipientType.TO, address);

					
					message.setSubject("Automation Testing  Report ");
					

					// Create the message part
					BodyPart messageBodyPart = new MimeBodyPart();
					// Now set the actual message
					//messageBodyPart.setText(emailContent);
					messageBodyPart.setContent(emailContent,"text/html");
					// Create a multipar message
					Multipart multipart = new MimeMultipart();
					// Set text message part
					multipart.addBodyPart(messageBodyPart);

					// Part two is attachment
					messageBodyPart = new MimeBodyPart();
					Thread.sleep(2000);
					

					// Part three is attachment
					messageBodyPart = new MimeBodyPart();
					String filenametext = System.getProperty("user.dir")+"/report/AutomationTestReport.html";
					DataSource source1 = new FileDataSource(filenametext);
					messageBodyPart.setDataHandler(new DataHandler(source1));
					messageBodyPart.setFileName("AutomationTestReport.html");
					multipart.addBodyPart(messageBodyPart);
					
					

					// Send the complete message parts
					message.setContent(multipart);
					Transport.send(message);


					System.out.println("Sent message successfully....");

				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}catch (InterruptedException Int)
				{
				//	Setup.log.debug("Exception in thread.sleep");
				}


				
			}

			private String getDateTime() {
				// TODO Auto-generated method stub
				return null;
			}

			
	}

