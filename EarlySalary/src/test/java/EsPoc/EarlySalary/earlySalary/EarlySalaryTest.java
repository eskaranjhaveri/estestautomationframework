package EsPoc.EarlySalary.earlySalary;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Hashtable;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
//import jdk.internal.org.objectweb.asm.commons.Method;

import EsPoc.EarlySalary.Email.EmailReportManager;
import EsPoc.EarlySalary.ExtentReports.ExtentTestManager;
import EsPoc.EarlySalary.ExtentReports.ExtentTestManager;
import EsPoc.EarlySalary.Common.CommonAction.CommonAction;
import EsPoc.EarlySalary.Common.CommonAction.ExcelReader;
import EsPoc.EarlySalary.Common.EsActions.earlySalaryActions;
import EsPoc.EarlySalary.Utilities.SuiteBase;
import java.lang.reflect.Method;


@Listeners(value = EmailReportManager.class)
public class EarlySalaryTest extends SuiteBase 
{


	CommonAction ca = new CommonAction();
	String methodName;
	
	private ExcelReader ex = new ExcelReader();
	private String getDataFromDataProviderSheet = System.getProperty("user.dir")
			+ rd.read_Configfile("earlySalary");
	
	
	@DataProvider(name= "TC_Login")
	public Object[][] getLoginData() throws InvalidFormatException, IOException
	{
		log.info("Data Provider Start ");
		Object [][] LoginData = ex.getDataingrid(getDataFromDataProviderSheet, "TC_AutoLogin_Data");
		log.info("Data Provider Ends ");
		return  LoginData;
	}
	
	@Test(dataProvider = "TC_Login", priority = 2 )
	public void VerifyValidLogin(Hashtable<String, String> ExcelSheetData, Method method) throws Exception 
	{
		System.out.println("\n" +"--------------------------------------TC_LoginSuccessfull");
		ExtentTestManager.startTest(method.getName(), "Test case to verify login by entring correct OTP");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		earlySalaryActions esAction = new earlySalaryActions(driver, wait);
		
		String ActualLoginText = esAction.TC_EarlySalaryLogin(methodName, ExcelSheetData, logger);		
		String ExpectedLogintext = ExcelSheetData.get("Expected_Text");
		System.out.println("Login text from sheet :" +ExpectedLogintext);
		ca.verifyAssertEqual(ActualLoginText, ExpectedLogintext, 
				"Unable to verify \" " + ExcelSheetData.get("Mobile_Number") + "\" message for login number.", 
				"\" " + ExcelSheetData.get("Mobile_Number") + "\" message for username verified successfully..", 
				ExpectedLogintext, logger);
	}
	
	
	@DataProvider(name="TC_LoginFail")
	public Object[][] getLoginDataForFail() throws InvalidFormatException, IOException
	{
		log.info("Data provider Start");
		Object[][] loginData = ex.getDataingrid(getDataFromDataProviderSheet, "TC_LoginFail_Data");
		log.info("Data Proivder Ends ");
		return loginData;
	}
	
	@Test(dataProvider="TC_LoginFail", priority= 1)
	public void VerifyLoginFailByWrongOTP(Hashtable<String, String> ExcelSheetData, Method method) throws Exception
	{
		System.out.println("\n" +"--------------------------------------TC_LoginFail" +"\n");
		ExtentTestManager.startTest(method.getName(), "Test case to try login by entring wrong OTP");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		earlySalaryActions esActions = new earlySalaryActions(driver, wait);

		String ActualloginFailText = esActions.TC_LoginFailConfirmation(methodName, ExcelSheetData, logger);
		String ExpectedLoginFailText = ExcelSheetData.get("Expected_Text");
		
		System.out.println("Actual text :" +ActualloginFailText);
		System.out.println("Expected text :" +ExpectedLoginFailText);
		
		ca.verifyAssertEqual(ActualloginFailText, ExpectedLoginFailText, 
				"Unable to verify \" " +ExcelSheetData.get("Expected_Text") + "\" message for login fail" , 
				
				"\" " +ExcelSheetData.get("Expected_Text") + "\" message verified successfully..", 
				ExpectedLoginFailText, logger);
		
	}
	
	@DataProvider(name="TC_Personal_Profile_Data")
	public Object[][] getPersonalProfileData() throws InvalidFormatException, IOException
	{
		log.info("Data provider Start");
		Object[][] personalProfileData = ex.getDataingrid(getDataFromDataProviderSheet, "TC_Personal_Profile_Data");
		log.info("Data provider Ends");
		return personalProfileData;
		
	}
	
	@Test(dataProvider = "TC_Personal_Profile_Data", priority = 3)
	public void VerifyPersonalProfileDataSelection(Hashtable<String, String> ExcelSheetData, Method method) throws Exception
	{
		System.out.println("\n" +"--------------------------------------TC_Personal Profile Data" +"\n");
		ExtentTestManager.startTest(method.getName(), "Test case to enter values in Personal Profile screen");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		earlySalaryActions esAction = new earlySalaryActions(driver, wait);
		
		String ActualTextConfirmationPersonalProfile = esAction.TC_PersonalProfileValidation(methodName, ExcelSheetData, logger);
		String ExpectedTextConfirmationPersonalProfile = ExcelSheetData.get("PersonalProfileConfirmation");
		
		ca.verifyAssertEqual(ActualTextConfirmationPersonalProfile, ExpectedTextConfirmationPersonalProfile,
				"Unable to verify \" "+ExcelSheetData.get("PersonalProfileConfirmation") + "\" confirmation text message ", 
				"\" "+ExcelSheetData.get("PersonalProfileConfirmation") + "\" confirmation text verified successfully..", 
				ExpectedTextConfirmationPersonalProfile, logger);
		
	}

}
